//
//  ApiHelper.swift
//  TranxitBase
//
//  Created by CSS on 12/01/18.
//  Copyright © 2018 Appoets. All rights reserved.
//

import UIKit
import Alamofire


enum serviceType :String{
    case POST = "POST"
    case GET = "GET"
    case PUT = "PUT"
}

typealias CompletionHandlerSuccess = (_ success:Bool,_ result :Any) -> Void
typealias CompletionHandlerFailre = (_ reason:String) -> Void
typealias CompletionHandlerLogout = (_ status :Bool) -> Void
typealias CompletionHandlerWithError = (_ error:Error) -> Void
typealias CSFailureWithInternet = (_ errorMessage : String) -> Void

class ApiHelper: NSObject {
    
    // var accessToken = UserDefaults.standard.string(forKey: UD_ACCESS_TOKEN) ?? ""
    static let apiIndstance = ApiHelper()
    let toast: JYToast = JYToast()
    
    //MARK: Api call for Update Functionalities.
    func dataUpdateWithApiCall(methodname:String,params :[String : AnyObject],type :(serviceType),Success :@escaping CompletionHandlerSuccess,failure :@escaping CompletionHandlerFailre,error :@escaping CompletionHandlerWithError,failureInternet: CSFailureWithInternet){
        activityIndicater(status: true)
        let accessToken = UserDefaults.standard.string(forKey: UD_ACCESS_TOKEN) ?? ""
        if (Reachability.isConnectedToNetwork()){
            var  head = ["":""]
            
            if (accessToken.isEmpty){
                 head = ["X-Requested-With": "XMLHttpRequest"]
            }else{
                head = [
                    "X-Requested-With": "XMLHttpRequest",
                    "Authorization":"Bearer \(accessToken)"]
            }
            let apiUrl = "\(BaseUrl)\(methodname)"
            switch type {
            case .GET:
                Alamofire.request(apiUrl, method: .get, parameters: params, encoding: URLEncoding.default, headers: head).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        print(response.response?.statusCode ?? 0)
                        if response.result.value != nil{
                            self.activityIndicater(status: false)
                            print(response.result.value ?? [:])
                            if(response.response?.statusCode == 200 || response.response?.statusCode == 201){
                                Success(true,response.result.value ?? [:])
                            }else{
                                if(self.checkErrorMesssage(response:response.result.value ?? [:]) == "unauthorized"){
                                    
                                }else{
                                    failure(self.checkErrorMesssage(response:response.result.value ?? [:]))
                                }
                            }
                        }else{
                            self.activityIndicater(status: false)
                            if(self.checkErrorMesssage(response:response.result.value ?? [:]) == "unauthorized"){
                                
                            }else{
                                failure(self.checkErrorMesssage(response:response.result.value ?? [:]))
                            }
                        }
                        break
                    case .failure(_):
                        self.activityIndicater(status: false)
                        print(response.result.error ?? [:])
                        error(response.result.error!)
                        break
                        
                    }
                }
                break
            case .POST:
                Alamofire.request(apiUrl, method: .post, parameters: params, encoding: URLEncoding.default, headers: head).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            self.activityIndicater(status: false)
                            print(response.result.value ?? [:])
                            if(response.response?.statusCode == 200 || response.response?.statusCode == 201){
                                Success(true,response.result.value ?? [:])
                            }else{
                                if(self.checkErrorMesssage(response:response.result.value ?? [:]) == "unauthorized"){
                                    
                                }else{
                                    failure(self.checkErrorMesssage(response:response.result.value ?? [:]))
                                }
                            }
                        }else{
                            self.activityIndicater(status: false)
                            failure(self.checkErrorMesssage(response:response.result.value ?? [:]))
                        }
                        
                        break
                    case .failure(_):
                        self.activityIndicater(status: false)
                        print(response.result.error ?? [:])
                        error(response.result.error!)
                        break
                        
                    }
                }
                
                break
            case .PUT:
                
                Alamofire.request(apiUrl, method: .put, parameters: params, encoding: URLEncoding.default, headers: head).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            self.activityIndicater(status: false)
                            print(response.result.value ?? [:])
                            Success(true,response.result.value ?? [:])
                        }else{
                            self.activityIndicater(status: false)
                            failure(self.checkErrorMesssage(response:response.result.value ?? [:]))
                        }
                        
                        break
                    case .failure(_):
                        self.activityIndicater(status: false)
                        print(response.result.error ?? [:])
                        error(response.result.error!)
                        break
                        
                    }
                }
                break
            }
        }
        else
        {
            self.activityIndicater(status: false)
            failureInternet("Please check your internet")
        }
    }
    
    
    //MARL: Error Message convert to String Alert
    func checkErrorMesssage (response :Any) ->String{
        var error = ""
        if(response is Dictionary<String,AnyObject>){
            let keys = Array((response as! Dictionary<String,AnyObject>).keys)
            if((response as! Dictionary<String,AnyObject>)[keys[0]] is String){
                error = (response as! Dictionary<String,AnyObject>)[keys[0]] as! String
            }else{
//                let responseArr = response as! NSDictionary
//                error = responseArr.object(forKey: keys[0]) as! String
            }
            print(keys)
            
        }else if(response is String){
            error = response as! String
        }else {
            let firstObject =  (response as! [Dictionary<String,AnyObject>])
            if(firstObject.count > 0){
                let dict = firstObject[0]
                let keys = Array(dict.keys)
                print(keys)
                if(dict[keys[0]] is String){
                    error = dict[keys[0]] as! String
                }
            }
        }
        return  error
    }
    //MARK: logout action
    func LogoutAction(){
        
    }
    //MARK:NULL remove String File
    func nullremove(value:Any?) -> AnyObject? {
        if value is NSNull {
            return "" as AnyObject?
        } else if(value == nil){
            return "" as AnyObject?
        }else{
            return value as AnyObject?
        }
    }
    
    //MARK:: ActivityIndicater Include
    func activityIndicater(status: Bool){
        DispatchQueue.main.async {
            let customView = Bundle.main.loadNibNamed("OverlayView", owner: self, options: nil)?.first as? OverlayView
            if(status){
                customView?.tag = 11
                customView?.frame = UIScreen.main.bounds
                customView?.activityIndicator.startAnimating()
                customView?.backgroundColor = UIColor.black.withAlphaComponent(0.50)
                UIApplication.shared.keyWindow?.addSubview(customView!)
                UIApplication.shared.beginIgnoringInteractionEvents()
            }else{
                let view = UIApplication.shared.keyWindow?.viewWithTag(11) as! OverlayView
                if(UIApplication.shared.keyWindow?.viewWithTag(11) != nil){
                    view.activityIndicator.stopAnimating()
                    view.removeFromSuperview()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            }
        }
    }
    
    //MARK: TOast
     func AlertMessage(message: String){
        toast.isShow(message)
    }
}
