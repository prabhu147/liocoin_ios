//
//  OverlayView.swift
//  Hotel Eats
//
//  Created by CSS on 28/02/18.
//  Copyright © 2018 Tranxit. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class OverlayView: UIView {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        themeAppearence()
    }
    
    func themeAppearence (){
        bgView.backgroundColor = UIColor.clear
        bgView.isOpaque = false
        activityIndicator.type = NVActivityIndicatorType(rawValue: NVActivityIndicatorType.ballPulse.rawValue)!
        activityIndicator.clipsToBounds = true
        activityIndicator.layer.shadowOffset = CGSize(width: 0, height: 0)
        activityIndicator.layer.shadowOpacity = 0.3
        activityIndicator.color = UIColor.secondary
        activityIndicator.startAnimating()
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "OverlayView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
