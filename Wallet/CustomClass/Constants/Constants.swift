//
//  Constants.swift
//  Wallet
//
//  Created by CSS on 28/09/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    static let string = Constants()
    let OK  = "OK"
    let appName = "LIO COIN"
    let coinStr = "COIN"
    let signUp = "SIGN UP"
    let login = "LOG IN"
    let accept = "ACCEPT"
    let continueStr = "CONTINUE"
    let declineStr = "DECLINE"
    let finishStr = "FINISH"
    let name = "Name"
    let password = "Password"
    let confirmPassword = "Confirm Password"
    let email = "Email ID"
    let noTranscationStr = "No Transcations"
    let transcationHistory = "Transcations History"
    let receiveStr = "RECEIVE"
    let sendStr = "SEND"
    let accountStr = "Account"
    let accountHeaderStr = "Account to Send"
    let toolsHeaderStr = "Tools"
    let settingsHeaderStr = "SETTINGS"
    let cancelStr = "CANCEL"
    let underStandStr = "I UNDERSTAND"
    let helpHeaderStr = "HELP"
    let walletHeaderStr = "Wallet"
    let currencyHeaderStr = "CURRENCY"
    let backupWalletStr = "BACKUP WALLET"
    let displayKeysHeaderStr = "DISPLAY PRIVATE KEYS"
    let dashHeaderStr = "Below are your Dash Public address and private key pairs."
    let exportStr = "EXPORT"
    let dashKeysStr = "DASH KEYS"
    let backupPharseHeaderStr = "VIEW BACKUP PHARSE"
    let passCodeHeaderStr = "SELECT NEW PIN"
    let confirmPin = "CONFIRM PIN"
    let addressHedaerStr = "YOUR CURRENT ETHEREUM ADDRESS:"
    let backWalletHeaderStr = "Would you like to backup your wallet now?"
    let backupContentStr = "Previous Backup: 11/09/2018 6:29 PM  \nNote: Backing up your wallet entails writing down your Backup Phrase. You will note be creating a ''backup'' copy of your wallet on this devices."
    let verifyStr = "Yes, I understand how Jaxx backup works"
    let pharseHeaderStr = "Please write down your Backup Pharse in the exact sequence below. On the next screen you will be asked to re-enter it in order to ensure accuracy"
    let successContentStr = "Keep  your written copy private in a secure and safe location. This is a key, should you ever need to restore your wallet."
    let successStr = "Success!"
    let successHedaerStr = "Confirm Your 12 -word Backup Phrase"
    let backupPharsetStr = "BACKUP PHRASE"
    let aboutHeaderStr = "ABOUT"
    let versionHedaerStr = "Version"
    let configurationHederStr = "Configuration"
    let websiteHedaerStr = "Website"
    let termsofserviceheaderStr = "Terms of services"
    let privacyHedaerStr = "Privacy Policy"
    let copyRightStr = "Copyright"
    let termsofserviceValueStr = "Show terms of services"
    let privacyValueStr = "Show privacy policy"
    let whatsnewValueStr = "Changelog"
    let whatsnewStr = "What is new"
    let agreestr = "By clicking accept, you confirm that you have read and agreed to our privacy policy and terms of Service"
    let agreeHeaderstr = "USER LICENSE AGREEMENT"
}

struct AlertString {
    static let string = AlertString()
    let deleteConfirmStr = "Are you sure you want to delete?"
    let logoutConfirmStr = "Are you sure you want to logout?"
    let enterMessageStr = "Please enter the Amount to Send"
    let enterAddressStr = "Please enter the Address to Send"
    let enterNameStr = "Please enter the Name"
    let enterAddAddressStr = "Please enter the Address"
    let enterAddCoinStr = "Please enter the coin"
    let coinSentStr = "Coin Sent"
    let wrongPinMessage = "Pin didn't match. Try again."
    let changePinMessage = "Pin Changed Successfully"
    let currencyChangeMessage = "Currency Changed Successfully"
    let addressCopyMessage = "Copied to clipBoard"
    let enableFingerMessage = "FingerPrint Enabled Successfully"
    let disableFingerMessage = "FingerPrint disabled Successfully"
    let enablelaunchingPinMessage = "Confirm the PIN code when launching app Enabled Successfully"
    let disablelaunchingPinMessage = "Confirm the PIN code when launching app disabled Successfully"
    let enterBackupWordsStr = "Enter your backupWords"
    let enablefingerPrintinApp = "Please configure your TouchID in Settings > Touch ID & Passcode"
    let enableCurrencyMessage = "Currency Enabled Successfully"
    let disableCurrencyMessage = "Currency disabled Successfully"
    
}
