//
//  CommonClass.swift
//  Wallet
//
//  Created by CSS on 28/09/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

//Button Type

enum CustomButtonType :Int {
    case WithShadow
    case WithOutCornerRadius
}

class customButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func withType (type:CustomButtonType,fontSize :CGFloat,BgColor :UIColor ,TextColor :UIColor){
        switch type {
        case .WithShadow:
            self.layer.cornerRadius = 4.0
            self.titleLabel?.font = UIFont.systemFont(ofSize: fontSize, weight: .medium)
            self.backgroundColor = BgColor
            self.layer.shadowOffset = CGSize(width:0,height:0)
            self.layer.shadowOpacity = 0.2
            
        case .WithOutCornerRadius:
            self.titleLabel?.font = UIFont.systemFont(ofSize: fontSize, weight: .medium)
            self.backgroundColor = BgColor
            self.layer.shadowOffset = CGSize(width:0,height:0)
            self.layer.shadowOpacity = 0.2
        }
        
        self.setTitleColor(TextColor, for: .normal)
    }
}


//MARK:Label Type declaration
enum LabelFontType :Int {
    case Default
    case Bold
    case Medium
}

class customLabel: UILabel {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func withType (type:LabelFontType,fontSize :CGFloat,fontcolor :UIColor){
        switch type {
        case .Default:
            self.backgroundColor = UIColor.clear
            self.font = UIFont.systemFont(ofSize: fontSize, weight: .regular)
            self.textColor = fontcolor
        case .Bold:
            self.backgroundColor = UIColor.clear
            self.font = UIFont.systemFont(ofSize: fontSize, weight: .bold)
            self.textColor = fontcolor
        case .Medium:
            self.backgroundColor = UIColor.clear
            self.font = UIFont.systemFont(ofSize: fontSize, weight: .medium)
            self.textColor = fontcolor
            
        }
    }
}

//MARK:UIVIEW Type declaration
enum ViewType :Int {
    case Default
    case Rounded
    case CornerRadius
    
}
class CustomView: UIView {
    func withType(type : ViewType,BgColr :UIColor,isBorder : Bool,borderWidth: CGFloat,isshadow :Bool){
        switch type {
        case .Default:
            break
        case .CornerRadius:
            self.layer.cornerRadius = 5.0
            break
        case .Rounded:
            self.layer.cornerRadius = self.frame.size.width / 2
            break
        }
        if isshadow{
            self.layer.shadowOffset = CGSize(width:0,height:0)
            self.layer.shadowColor = UIColor.gray.cgColor
            self.layer.shadowOpacity = 5.0
        }
        if isBorder{
            self.layer.borderColor = UIColor.lightGray.cgColor
            self.layer.borderWidth = 0.5
        }
        self.layer.masksToBounds = true
        self.backgroundColor = BgColr
    }
    
}

//MARK:UITextField Type declaration

enum TextFieldType :Int {
    case Default
    case bordered
}

class customTextField: UITextField {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func withType (type:TextFieldType,fontSize :CGFloat,textField :UITextField,fontcolor :UIColor,placeholderTxt :String){
        switch type {
        case .bordered:
            let bottomBorder = CALayer()
            bottomBorder.frame =  CGRect(x: 0.0, y: textField.frame.size.height - 1, width: textField.frame.size.width, height: 1)
            textField.font = UIFont.boldSystemFont(ofSize: fontSize)
            textField.textColor = fontcolor;
            bottomBorder.backgroundColor = UIColor.white.cgColor
            textField.layer.addSublayer(bottomBorder)
        case .Default:
            let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 20.0, height: textField.frame.size.height))
            textField.leftView = leftView
            textField.leftViewMode = .always
            textField.font = UIFont.boldSystemFont(ofSize: fontSize)
            textField.textColor = fontcolor;
            textField.attributedPlaceholder = NSAttributedString(string:placeholderTxt,
                                                                 attributes: [NSAttributedStringKey.foregroundColor:fontcolor])
        }
    }
}

class CommonClass: NSObject {
    
    //Create QRCode Image
    class func CreateQrCodeForyourString (string:String)-> UIImage{
        let stringData = string.data(using: .utf8, allowLossyConversion: false)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter?.setValue(stringData, forKey: "inputMessage")
        filter?.setValue("H", forKey: "inputCorrectionLevel")
        let qrCIImage = filter?.outputImage
        let colorFilter = CIFilter(name: "CIFalseColor")!
        colorFilter.setDefaults()
        colorFilter.setValue(qrCIImage, forKey: "inputImage")
        colorFilter.setValue(CIColor(red: 0, green: 0, blue: 0), forKey: "inputColor0")
        colorFilter.setValue(CIColor(red: 1, green: 1, blue: 1), forKey: "inputColor1")
        
        let codeImage = UIImage(ciImage: (colorFilter.outputImage!.transformed(by: CGAffineTransform(scaleX: 5, y: 5))))
        return codeImage
    }
    
    class func coloredImage(image:UIImage, color: UIColor) -> UIImage? {
       
        let backgroundSize = image.size
        UIGraphicsBeginImageContextWithOptions(backgroundSize, false, UIScreen.main.scale)
        let ctx = UIGraphicsGetCurrentContext()!
        var backgroundRect=CGRect()
        backgroundRect.size = backgroundSize
        backgroundRect.origin.x = 0
        backgroundRect.origin.y = 0
        
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        color.getRed(&r, green: &g, blue: &b, alpha: &a)
        ctx.setFillColor(red: r, green: g, blue: b, alpha: a)
        ctx.fill(backgroundRect)
        
        var imageRect = CGRect()
        imageRect.size = image.size
        imageRect.origin.x = ((backgroundSize.width) - image.size.width) / 2
        imageRect.origin.y = ((backgroundSize.height) - image.size.height) / 2
    
        ctx.translateBy(x: 0, y: (backgroundSize.height))
        ctx.scaleBy(x: 1.0, y: -1.0)
        
        ctx.setBlendMode(.destinationIn)
        ctx.draw(image.cgImage!, in: imageRect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    class func dottedLine(view: UIView,isDotted : Bool){
        if isDotted {
            let line = CAShapeLayer()
            line.path = UIBezierPath(roundedRect: view.bounds, cornerRadius:5.0).cgPath
            line.frame = view.bounds
            line.strokeColor = UIColor.buttonColor.cgColor
            line.fillColor = nil
            line.lineWidth = 3.0
            line.lineDashPattern = [4, 4]
            view.layer.addSublayer(line)
        }else{
            for layer in view.layer.sublayers! {
                if (layer is CAShapeLayer) {
                    layer.removeFromSuperlayer()
                }
            }
        }
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 5.0
        view.layer.shadowOffset = CGSize(width:0,height:0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.5
    }
    
    class func showAlert(title: String? = nil,
                         message: String? = nil,
                         viewController: UIViewController,
                         completion: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.string.OK, style: UIAlertAction.Style.destructive,
                                      handler: completion))
        alert.addAction(UIAlertAction(title: Constants.string.cancelStr, style: UIAlertAction.Style.cancel,
                                      handler:nil))
        viewController.present(alert, animated: true, completion:nil)
    }
    
}
