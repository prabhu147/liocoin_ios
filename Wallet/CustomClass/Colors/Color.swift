//
//  Color.swift
//  Wallet
//
//  Created by CSS on 28/09/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

enum Color : Int {
    
    case primary = 1
    case secondary = 2
    case textColor = 3
    case buttonColor = 4
    case cellBgColor = 5
    case placeholder = 6
     case buttonTextColor = 7
    
    static func valueFor(id : Int)->UIColor?{
        switch id {
        case self.primary.rawValue:
            return .primary
        case self.secondary.rawValue:
            return .secondary
        case self.textColor.rawValue:
            return .textColor
        case self.buttonColor.rawValue:
            return .buttonColor
        case self.cellBgColor.rawValue:
            return .cellBgColor
        case self.placeholder.rawValue:
            return .placeholder
        case self.buttonTextColor.rawValue:
            return .buttonTextColor
        default:
            return nil
        }
    }
}

extension UIColor {
    
    static var primary : UIColor {
        return UIColor(red: 18/255, green: 21/255, blue: 27/255, alpha: 1)
    }
    static var secondary : UIColor {
        return UIColor(red: 33/255, green: 40/255, blue: 50/255, alpha: 1)
    }
    static var textColor : UIColor {
        return UIColor(red: 18/255, green: 18/255, blue: 18/255, alpha: 1)
    }
    static var buttonColor : UIColor {
        return UIColor(red: 198/255, green: 172/255, blue: 100/255, alpha: 1)
    }
    static var cellBgColor : UIColor {
        return UIColor(red: 54/255, green: 54/255, blue: 54/255, alpha: 1)
    }
    static var placeholder : UIColor {
        return UIColor(red: 109/255, green: 109/255, blue: 109/255, alpha: 1)
    }
    static var buttonTextColor : UIColor {
        return UIColor(red: 112/255, green: 112/255, blue: 112/255, alpha: 1)
    }
    func UInt()->UInt32{
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            var colorAsUInt : UInt32 = 0
            colorAsUInt += UInt32(red * 255.0) << 16 +
                UInt32(green * 255.0) << 8 +
                UInt32(blue * 255.0)
            return colorAsUInt
        }
        return 0xCC6699
    }
}

