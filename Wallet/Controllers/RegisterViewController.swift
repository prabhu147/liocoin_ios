//
//  RegisterViewController.swift
//  Wallet
//
//  Created by CSS on 28/09/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    //Outlets
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var headerLbl: customLabel!
    @IBOutlet weak var nameBaseView: CustomView!
    @IBOutlet weak var nameTxt: customTextField!
    @IBOutlet weak var confirmPassTxt: customTextField!
    @IBOutlet weak var confirmPassView: CustomView!
    @IBOutlet weak var passwordTxt: customTextField!
    @IBOutlet weak var passwordBaseView: CustomView!
    @IBOutlet weak var emailTxt: customTextField!
    @IBOutlet weak var emailBaseView: CustomView!
    @IBOutlet weak var nextBtnOut: customButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
        headerLbl.text = Constants.string.signUp
        nextBtnOut.setTitle(Constants.string.signUp, for: .normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func themeAppearence()  {
        self.view.backgroundColor = UIColor.primary
        headerView.withType(type: .Default, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        nameTxt.withType(type: .Default, fontSize: 20, textField: nameTxt, fontcolor: .placeholder, placeholderTxt:Constants.string.name)
        emailTxt.withType(type: .Default, fontSize: 20, textField: emailTxt, fontcolor: .placeholder, placeholderTxt:Constants.string.email)
        passwordTxt.withType(type: .Default, fontSize: 20, textField: passwordTxt, fontcolor: .placeholder, placeholderTxt:Constants.string.password)
        confirmPassTxt.withType(type: .Default, fontSize: 20, textField: confirmPassTxt, fontcolor: .placeholder, placeholderTxt:Constants.string.confirmPassword)
        nameBaseView.withType(type: .CornerRadius, BgColr: .cellBgColor, isBorder: false, borderWidth: 0.0, isshadow: true)
        emailBaseView.withType(type: .CornerRadius, BgColr: .cellBgColor, isBorder: false, borderWidth: 0.0, isshadow: true)
        passwordBaseView.withType(type: .CornerRadius, BgColr: .cellBgColor, isBorder: false, borderWidth: 0.0, isshadow: true)
        confirmPassView.withType(type: .CornerRadius, BgColr: .cellBgColor, isBorder: false, borderWidth: 0.0, isshadow: true)
        headerLbl.withType(type: .Default, fontSize: 22, fontcolor: .white)
        nextBtnOut.withType(type: .WithShadow, fontSize: 20, BgColor: .buttonColor, TextColor: .black)
    }
    
    @IBAction func backBtnAct(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closeBtnAct(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextBtnAct(_ sender: Any) {
        self.performSegue(withIdentifier: "HomeSegue", sender: self)
    }
}
