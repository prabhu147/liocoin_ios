//
//  DisplayKeysViewController.swift
//  Wallet
//
//  Created by CSS on 04/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

class DisplayKeysCell:UITableViewCell {
    @IBOutlet weak var contentLbl: customLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        contentLbl.withType(type: .Bold, fontSize: 20, fontcolor: .white)
    }
}

class DisplayKeysViewController: UIViewController {

    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var headerLbl: customLabel!
    @IBOutlet weak var displayKeysTable: UITableView!
    var displayKeysArr = ["Display Dash Keys"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
    }
    
    func themeAppearence(){
        self.view.backgroundColor = UIColor.primary
        self.displayKeysTable.backgroundColor = UIColor.clear
        headerLbl.withType(type: .Bold, fontSize: 22, fontcolor: .white)
        headerView.withType(type: .Default, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        headerLbl.text = Constants.string.backupWalletStr
    }
    
    @IBAction func backBtnAct(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

// MARK:- UITableView
extension DisplayKeysViewController : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayKeysArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DisplayKeysCell", for: indexPath) as! DisplayKeysCell
        let separatorLine = UIImageView.init(frame: CGRect(x: 0, y: cell.frame.height-1, width: cell.frame.width, height: 1))
        separatorLine.backgroundColor = UIColor.cellBgColor
        cell.addSubview(separatorLine)
        cell.tintColor = UIColor.white
        cell.backgroundColor = UIColor.clear
        cell.contentLbl.text = displayKeysArr[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "DashKeysViewController") as! DashKeysViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
