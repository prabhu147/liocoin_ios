//
//  BackPharseViewController.swift
//  Wallet
//
//  Created by CSS on 05/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

class BackPharseViewController: UIViewController {
    
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var headerLbl: customLabel!
    @IBOutlet weak var pharseLbl: customLabel!
    @IBOutlet weak var headerContentLbl: customLabel!
    @IBOutlet weak var nextBtnOut: customButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
    }
    
    func themeAppearence(){
        self.view.backgroundColor = UIColor.primary
        headerLbl.withType(type: .Bold, fontSize: 22, fontcolor: .white)
        headerContentLbl.withType(type: .Medium, fontSize: 18, fontcolor: .white)
        pharseLbl.withType(type: .Medium, fontSize: 18, fontcolor: .buttonColor)
        headerView.withType(type: .Default, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        nextBtnOut.withType(type: .WithShadow, fontSize: 20, BgColor: .buttonColor, TextColor: .black)
        nextBtnOut.setTitle(Constants.string.continueStr, for: .normal)
        headerLbl.text = Constants.string.backupWalletStr
        headerContentLbl.text = Constants.string.pharseHeaderStr
        pharseLbl.text = "behave bone cannon confirm couple crane easily elder exotic grunt host level orbit over rate ridge senior shadow table truth unfold utility vapor voice"
    }
    
    @IBAction func backBtnAct(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextBtnAct(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmPharseViewController") as! ConfirmPharseViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }

}
