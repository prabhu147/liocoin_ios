//
//  WarrningViewController.swift
//  Wallet
//
//  Created by CSS on 04/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

class WarrningViewController: UIViewController {

    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var headerLbl: customLabel!
    @IBOutlet weak var warrningBtnOut: customButton!
    @IBOutlet weak var warrningContentLbl: customLabel!
    @IBOutlet weak var understandBtnOut: customButton!
    @IBOutlet weak var cancelBtnOut: customButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
    }
    
    func themeAppearence(){
        self.view.backgroundColor = UIColor.primary
        cancelBtnOut.withType(type: .WithShadow, fontSize: 20, BgColor: .buttonColor, TextColor: .white)
        cancelBtnOut.setTitle(Constants.string.cancelStr, for: .normal)
        understandBtnOut.withType(type: .WithShadow, fontSize: 20, BgColor: .buttonColor, TextColor: .white)
        understandBtnOut.setTitle(Constants.string.underStandStr, for: .normal)
        headerLbl.withType(type: .Bold, fontSize: 22, fontcolor: .white)
        headerView.withType(type: .Default, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        warrningBtnOut.withType(type: .WithOutCornerRadius, fontSize: 25, BgColor: .clear, TextColor: .white)
        headerLbl.text = Constants.string.backupPharseHeaderStr
    }
    
    
    @IBAction func backBtnAct(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func underStandBtnAct(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewBackupPharseViewController") as! ViewBackupPharseViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
