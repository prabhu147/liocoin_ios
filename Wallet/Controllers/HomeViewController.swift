//
//  HomeViewController.swift
//  Wallet
//
//  Created by CSS on 01/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

//Currency CollectionCell
class CurrencyCollectionCell: UICollectionViewCell {
@IBOutlet weak var currencyBaseView: CustomView!
@IBOutlet weak var currencyValueLbl: customLabel!
@IBOutlet weak var currencyImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        currencyBaseView.withType(type: .CornerRadius, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        currencyValueLbl.withType(type: .Medium, fontSize: 16, fontcolor: .white)
    }
}

//Address TableViewCell
class AddressCell: UITableViewCell {
    @IBOutlet weak var addressBtn: customButton!
    @IBOutlet weak var addressHeaderLbl: customLabel!
    @IBOutlet weak var lineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lineView.backgroundColor = UIColor.placeholder
        addressHeaderLbl.withType(type: .Medium, fontSize: 16, fontcolor: .white)
        addressBtn.titleLabel?.textColor = UIColor.white
    }
}
//QRCode TableViewCell
class QRCodeCell: UITableViewCell {
    @IBOutlet weak var currencyValueBaseView: CustomView!
    @IBOutlet weak var currencyValueLbl: customLabel!
    @IBOutlet weak var totalAmtLbl: customLabel!
    @IBOutlet weak var totalAmtInUsdLbl: customLabel!
    @IBOutlet weak var transcationHeaderLbl: customLabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var qrCodeBtnOut: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lineView.backgroundColor = UIColor.placeholder
        currencyValueBaseView.withType(type: .Default, BgColr: .clear, isBorder: false, borderWidth: 0.0, isshadow: true)
        currencyValueLbl.withType(type: .Medium, fontSize: 14, fontcolor: .buttonColor)
        totalAmtLbl.withType(type: .Medium, fontSize: 18, fontcolor: .buttonColor)
        totalAmtInUsdLbl.withType(type: .Medium, fontSize: 16, fontcolor: .white)
        transcationHeaderLbl.withType(type: .Medium, fontSize: 18, fontcolor: .white)
    }
}
//Transcation TableViewCell
class TranscationCell: UITableViewCell {
    @IBOutlet weak var noTranscationLbl: customLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        noTranscationLbl.withType(type: .Medium, fontSize: 16, fontcolor: .placeholder)
    }
}

class HomeViewController: UIViewController  {
    
   //Outlets
    @IBOutlet weak var menuView: CustomView!
    @IBOutlet weak var currencyCollectionView: UICollectionView!
    @IBOutlet weak var transcationTableView: UITableView!
    @IBOutlet weak var sendBtn: customButton!
    @IBOutlet weak var receiveBtn: customButton!
    @IBOutlet weak var topViewLbl: customLabel!
    
    var currencyListArr = [["currency":"BTC","currencyimage":"bitcoin"],["currency":"ETH","currencyimage":"eth"],["currency":"DSH","currencyimage":"dash"],["currency":"DSH","currencyimage":"dash"]]
    var selectedIndex = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func themeAppearence()  {
        self.view.backgroundColor = UIColor.primary
        sendBtn.setTitle(Constants.string.sendStr, for: .normal)
        receiveBtn.setTitle(Constants.string.receiveStr, for: .normal)
        menuView.withType(type: .Default, BgColr: .clear, isBorder: false, borderWidth: 0.0, isshadow: true)
        sendBtn.withType(type: .WithShadow, fontSize: 18, BgColor: .secondary, TextColor: .white)
        receiveBtn.withType(type: .WithShadow, fontSize: 18, BgColor: .secondary, TextColor: .white)
        topViewLbl.withType(type: .Bold, fontSize: 28, fontcolor: .white)
        topViewLbl.text = Constants.string.appName
    }

    @IBAction func menuBtnAct(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"LeftMenuViewController" )as! LeftMenuViewController
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func qrCodeBtnAct(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func resciveBtnAct(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "RECEIVEViewController") as! RECEIVEViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func sendBtnAct(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SendViewController") as! SendViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

//Collection View Delegate
extension HomeViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currencyListArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = currencyCollectionView.dequeueReusableCell(withReuseIdentifier: "CurrencyCollectionCell", for: indexPath) as! CurrencyCollectionCell
        if selectedIndex == indexPath.item {
            DispatchQueue.main.async {
                CommonClass.dottedLine(view: cell.currencyBaseView , isDotted: true)
            }
        }else{
            CommonClass.dottedLine(view: cell.currencyBaseView , isDotted: false)
        }
        cell.currencyImage.image = UIImage(named: currencyListArr[indexPath.item]["currencyimage"]!)
        cell.currencyValueLbl.text =  currencyListArr[indexPath.item]["currencyimage"] ?? ""
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width:currencyCollectionView.frame.size.width/3 , height: currencyCollectionView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        selectedIndex = indexPath.item
        currencyCollectionView.reloadData()
    }
}

//Table View Delegate
extension HomeViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else if section == 1 {
            return 1
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell", for: indexPath) as!  AddressCell
            cell.addressHeaderLbl.text = "Your current BitCoin Address:"
            cell.addressBtn.tag = indexPath.row
            cell.addressBtn .setTitle("0x24b8ce20aeb2e68168e389bdb5f4b26bb77ac018", for: .normal)
            return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "QRCodeCell", for: indexPath) as!  QRCodeCell
            cell.currencyValueLbl.text = "BTC"
            cell.totalAmtLbl.text = "0.0"
            cell.totalAmtInUsdLbl.text = "US$ 0.00"
            cell.transcationHeaderLbl.text = Constants.string.transcationHistory
            cell.qrCodeBtnOut.setImage(CommonClass.CreateQrCodeForyourString(string:"sample"), for: .normal)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TranscationCell", for: indexPath) as!  TranscationCell
            cell.noTranscationLbl.text = Constants.string.noTranscationStr
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.section == 0 {
            return 80
        }else if indexPath.section  == 1 {
            return 165
        }else{
            return 50
        }
    }
}



