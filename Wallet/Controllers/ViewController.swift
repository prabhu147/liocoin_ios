//
//  ViewController.swift
//  Wallet
//
//  Created by CSS on 28/09/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

class InitialViewTableViewCell:UITableViewCell {
    @IBOutlet weak var contentLbl: customLabel!
    @IBOutlet weak var selectedImg: UIImageView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        contentLbl.withType(type: .Bold, fontSize: 20, fontcolor: .white)
    }
}

class ViewController: UIViewController {
    
    //Outlets
@IBOutlet weak var initialTableView: UITableView!
@IBOutlet weak var nextBtnOut: customButton!

    
    var initialViewArr = [["name":Constants.string.signUp,"selectedIndex":"1"],["name":Constants.string.login,"selectedIndex":"0"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialTableView.layoutMargins = UIEdgeInsets.zero
        initialTableView.separatorInset = UIEdgeInsets.zero
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       themeAppearence()
    }
    
    func themeAppearence(){
        self.view.backgroundColor = UIColor.primary
        self.initialTableView.backgroundColor = UIColor.clear
        nextBtnOut.setTitle(Constants.string.accept, for: .normal)
        nextBtnOut.withType(type: .WithShadow, fontSize: 18, BgColor: .buttonColor, TextColor: .black)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func nextBtnAct(_ sender: Any) {
    }
}

// MARK:- UITableView
extension ViewController : UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return initialViewArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InitialViewTableViewCell", for: indexPath) as! InitialViewTableViewCell
        let separatorLine = UIImageView.init(frame: CGRect(x: 0, y: cell.frame.height-1, width: cell.frame.width, height: 1))
        separatorLine.backgroundColor = UIColor.white
        cell.addSubview(separatorLine)
        if indexPath.row == 0 {
            separatorLine.isHidden = false
        }else{
            separatorLine.isHidden = true
        }
        cell.tintColor = UIColor.white
        cell.backgroundColor = UIColor.cellBgColor
        cell.contentLbl.text = initialViewArr[indexPath.row]["name"]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.initialTableView.frame.size.height/2
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if indexPath.row ==  0 {
            self.performSegue(withIdentifier: "LoginSegue", sender: self)
        }else{
            self.performSegue(withIdentifier: "RegisterSegue", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath){
        if indexPath.row ==  0 {
            self.performSegue(withIdentifier: "LoginSegue", sender: self)
        }else{
            self.performSegue(withIdentifier: "RegisterSegue", sender: self)
        }
    }
}

