//
//  HelpViewController.swift
//  Wallet
//
//  Created by CSS on 03/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

class HelpTableViewCell:UITableViewCell {
    @IBOutlet weak var contentLbl: customLabel!
    @IBOutlet weak var contentBaseView: CustomView!
    override func awakeFromNib() {
        super.awakeFromNib()
        contentLbl.withType(type: .Bold, fontSize: 15, fontcolor: .white)
        contentBaseView.withType(type: .CornerRadius, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
    }
}

class HelpViewController: UIViewController {

    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var headerLbl: customLabel!
    @IBOutlet weak var helpTable: UITableView!
    var helpListArr = ["How do I send and receive funds?","How can I convert between coins/tokens?","How do backup Jaxx?","How do I sync Jaxx across devices?","How long will it take to receive funds after they’ve been sent?","Does it cost anything to send and receive funds?","Can I point my mining rig payouts to Jaxx?","What’s an HD wallet and why does it"]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
    }
    
    func themeAppearence(){
        self.view.backgroundColor = UIColor.primary
        self.helpTable.backgroundColor = UIColor.clear
        headerLbl.withType(type: .Bold, fontSize: 22, fontcolor: .white)
        headerView.withType(type: .Default, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        headerLbl.text = Constants.string.helpHeaderStr
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnAct(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

// MARK:- UITableView
extension HelpViewController : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return helpListArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpTableViewCell", for: indexPath) as! HelpTableViewCell
        cell.backgroundColor = UIColor.clear
        cell.contentLbl.text = helpListArr[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
