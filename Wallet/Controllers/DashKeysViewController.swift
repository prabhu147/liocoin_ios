//
//  DashKeysViewController.swift
//  Wallet
//
//  Created by CSS on 04/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit


class DashKeysHeaderCell: UITableViewCell {
    @IBOutlet weak var dashKeysHeaderLbl: customLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        dashKeysHeaderLbl.withType(type: .Medium, fontSize: 16, fontcolor: .white)
    }
}

class DashKeysCell: UITableViewCell {
    @IBOutlet weak var publicAddressLbl: customLabel!
    @IBOutlet weak var amtLbl: customLabel!
    @IBOutlet weak var privateAddressLbl: customLabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var dashImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lineView.backgroundColor = UIColor.placeholder
        publicAddressLbl.withType(type: .Medium, fontSize: 14, fontcolor: .white)
        amtLbl.withType(type: .Medium, fontSize: 14, fontcolor: .white)
        privateAddressLbl.withType(type: .Medium, fontSize: 14, fontcolor: .white)
    }
}

class DashKeysViewController: UIViewController {

    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var headerLbl: customLabel!
    @IBOutlet weak var dashKeysTable: UITableView!
    @IBOutlet weak var nextBtnOut: customButton!
    
    var dashKeysArr = [["privatekey":"d1fdbf52c2d50bebb1991982f51bbc3e92ff781980979381cbcc19a32aaf29ab","publickey":"0xc3785ca6f36b9d2128ef74b0a63a40404138ba6b"],["privatekey":"d1fdbf52c2d50bebb1991982f51bbc3e92ff781980979381cbcc19a32aaf29ab","publickey":"0xc3785ca6f36b9d2128ef74b0a63a40404138ba6b"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
    }
    
    func themeAppearence(){
        self.view.backgroundColor = UIColor.primary
        self.dashKeysTable.backgroundColor = UIColor.clear
        nextBtnOut.withType(type: .WithShadow, fontSize: 20, BgColor: .buttonColor, TextColor: .white)
        nextBtnOut.setTitle(Constants.string.exportStr, for: .normal)
        headerLbl.withType(type: .Bold, fontSize: 22, fontcolor: .white)
        headerView.withType(type: .Default, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        headerLbl.text = Constants.string.dashKeysStr
    }
    
    @IBAction func backBtnAct(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//Table View Delegate
extension DashKeysViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else {
            return dashKeysArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DashKeysHeaderCell", for: indexPath) as!  DashKeysHeaderCell
            let separatorLine = UIImageView.init(frame: CGRect(x: 0, y: cell.frame.height-1, width: cell.frame.width, height: 1))
            separatorLine.backgroundColor = UIColor.cellBgColor
            cell.addSubview(separatorLine)
            cell.dashKeysHeaderLbl.text = Constants.string.dashHeaderStr
            cell.tintColor = UIColor.white
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DashKeysCell", for: indexPath) as!  DashKeysCell
            cell.dashImage.image = #imageLiteral(resourceName: "dash")
            cell.amtLbl.text = "0.000000000 DASH"
            cell.publicAddressLbl.text = "Public Address \n" + dashKeysArr[indexPath.row]["publickey"]!
            cell.privateAddressLbl.text = "Private Key \n" + dashKeysArr[indexPath.row]["privatekey"]!
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.section == 0 {
            return 70
        }else {
            return 180
        }
    }
}

