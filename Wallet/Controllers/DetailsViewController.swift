//
//  DetailsViewController.swift
//  Wallet
//
//  Created by CSS on 04/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var detailsBaseView: CustomView!
    @IBOutlet weak var qrImage: UIImageView!
    @IBOutlet weak var addressLbl: customLabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var addressHedaerLbl: customLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
    }
    
    func themeAppearence(){
        self.view.backgroundColor = UIColor.primary
        addressHedaerLbl.withType(type: .Medium, fontSize: 20, fontcolor: .white)
        addressHedaerLbl.text = Constants.string.addressHedaerStr
        addressLbl.text = "0xc3785ca6f36b9d2128ef74b0a63a40404138ba6b"
        addressLbl.withType(type: .Medium, fontSize: 16, fontcolor: .white)
        lineView.backgroundColor = UIColor.placeholder
        detailsBaseView.withType(type: .CornerRadius, BgColr: .cellBgColor, isBorder: false, borderWidth: 0.0, isshadow: true)
        qrImage.image = CommonClass.CreateQrCodeForyourString(string: "sample")
    }
    
    @IBAction func closeBtnAct(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
