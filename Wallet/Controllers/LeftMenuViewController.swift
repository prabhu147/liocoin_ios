//
//  LeftMenuViewController.swift
//  Wallet
//
//  Created by CSS on 01/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

class HederTableViewCell: UITableViewCell {
    @IBOutlet weak var menuLbl: customLabel!
    @IBOutlet weak var lineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        menuLbl.withType(type: .Medium, fontSize: 16, fontcolor: .white)
        lineView.backgroundColor = UIColor.placeholder
    }
}


//TableViewOutlet
class menuTableViewCell: UITableViewCell {
    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var menuLbl: customLabel!
    @IBOutlet weak var lineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        menuLbl.withType(type: .Medium, fontSize: 16, fontcolor: .white)
        lineView.backgroundColor = UIColor.placeholder
    }
}


class LeftMenuViewController: UIViewController {

   
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var menuView: CustomView!
    
    var menuArr = [["name":"Tools","image":"tools"],["name":"Settings","image":"settings"],["name":"Help","image":"help"],["name":"About","image":"about"],["name":"Wallet","image":"wallet"],["name":"Currency","image":"currency"],["name":"Logout","image":"logout"]]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        menuTableView.backgroundColor = UIColor.clear
        menuView.withType(type: .Default, BgColr: .clear, isBorder: false, borderWidth: 0.0, isshadow: true)
        self.view.backgroundColor = UIColor.primary
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuBtnAct(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
//    @IBAction func MenuBtnOuterTabAction(_ sender: UITapGestureRecognizer) {
//        NotificationCenter.default.post(name: Notification.Name("MenuOutterTabDidTab"), object: nil)
//
//    }
}

//extension LeftMenuViewController : UIGestureRecognizerDelegate{
//    //MARK: gesture Action identification
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
//        if (touch.view?.isDescendant(of: self.menuTableView))!{
//            return false
//        }
//        return true
//    }
//}

extension LeftMenuViewController:UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else{
            return menuArr.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "menuTableViewCell", for: indexPath) as! menuTableViewCell
            cell.menuLbl.text = menuArr[indexPath.row]["name"] ?? ""
            cell.menuImage.image = UIImage(named: menuArr[indexPath.row]["image"] ?? "")
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HederTableViewCell", for: indexPath) as! HederTableViewCell
            cell.menuLbl.text = "MENU"
             return cell
        }
      
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            switch  menuArr[indexPath.row]["name"] {
            case "Tools":
                let controller = self.storyboard?.instantiateViewController(withIdentifier:"ToolsViewController" )as! ToolsViewController
                self.navigationController?.pushViewController(controller, animated: true)
                break
            case "Settings":
                let  controller = self.storyboard?.instantiateViewController(withIdentifier:"SettingsViewController" )as! SettingsViewController
                self.navigationController?.pushViewController(controller, animated: true)
                break
            case "Help":
                let  controller = self.storyboard?.instantiateViewController(withIdentifier:"HelpViewController" )as! HelpViewController
                self.navigationController?.pushViewController(controller, animated: true)
                break
            case "About":
                let  controller = self.storyboard?.instantiateViewController(withIdentifier:"AboutViewController" )as! AboutViewController
                self.navigationController?.pushViewController(controller, animated: true)
                break
            case "Wallet":
                let  controller = self.storyboard?.instantiateViewController(withIdentifier:"WalletViewController" )as! WalletViewController
                self.navigationController?.pushViewController(controller, animated: true)
                break
            case "Currency":
                let  controller = self.storyboard?.instantiateViewController(withIdentifier:"CurrencyViewController" )as! CurrencyViewController
                self.navigationController?.pushViewController(controller, animated: true)
                break
            case "Logout":
                CommonClass.showAlert(title: Constants.string.appName, message: AlertString.string.logoutConfirmStr , viewController: self, completion:  { (UIAlertAction) in
                    
                    })
                break
            default:
                break
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
}
