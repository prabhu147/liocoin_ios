//
//  SendViewController.swift
//  Wallet
//
//  Created by CSS on 03/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

class SendViewController: UIViewController {

    //OUTLETS
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var headerLbl: customLabel!
    @IBOutlet weak var txtBaseView: CustomView!
    @IBOutlet weak var currencyBtn: customButton!
    @IBOutlet weak var addressTxt: customTextField!
    @IBOutlet weak var addressLbl: customLabel!
    @IBOutlet weak var addressHeaderLbl: customLabel!
    @IBOutlet weak var addressBaseView: CustomView!
    @IBOutlet weak var sendBtn: customButton!
    @IBOutlet weak var amountBaseView: CustomView!
    @IBOutlet weak var amountTxt: customTextField!
     @IBOutlet weak var scannerBtn: customButton!
    
    @IBOutlet weak var amountHeaderLbl: customLabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
        stringUpdate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func themeAppearence()  {
        self.view.backgroundColor = UIColor.primary
        headerView.withType(type: .Default, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        headerLbl.withType(type: .Bold, fontSize: 22, fontcolor: .white)
        amountHeaderLbl.withType(type: .Bold, fontSize: 20, fontcolor: .white)
        txtBaseView.withType(type: .CornerRadius, BgColr: .white, isBorder: false, borderWidth: 0.0, isshadow: true)
         amountBaseView.withType(type: .CornerRadius, BgColr: .white, isBorder: false, borderWidth: 0.0, isshadow: true)
        currencyBtn.withType(type: .WithOutCornerRadius, fontSize: 18, BgColor: .secondary, TextColor: .white)
        addressTxt.withType(type: .Default, fontSize: 16, textField: addressTxt, fontcolor: .black, placeholderTxt: "Enter the Receiving Address")
        amountTxt.withType(type: .Default, fontSize: 16, textField: amountTxt, fontcolor: .black, placeholderTxt: "Enter Amount")
        addressHeaderLbl.withType(type: .Bold, fontSize: 14, fontcolor: .white)
        addressLbl.withType(type: .Bold, fontSize: 14, fontcolor: .white)
        addressBaseView.withType(type: .CornerRadius, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        
        sendBtn.withType(type: .WithShadow, fontSize: 20, BgColor: .buttonColor, TextColor: .black)
    }
    
    func stringUpdate()  {
        headerLbl.text = Constants.string.sendStr
        sendBtn.setTitle(Constants.string.sendStr, for: .normal)
        amountHeaderLbl .text = Constants.string.accountHeaderStr
        addressHeaderLbl.text = "Transcation Fee: " + "0.0000001"
        currencyBtn.setTitle("LIO", for: .normal)
        addressLbl.text = "You can send upto: " + "192087.649950918"
    }
    
    @IBAction func sendBtnAct(_ sender: Any) {
        
    }
    
    @IBAction func backBtnAct(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
