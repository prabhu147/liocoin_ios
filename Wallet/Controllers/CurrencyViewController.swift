//
//  CurrencyViewController.swift
//  Wallet
//
//  Created by CSS on 04/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

//Address TableViewCell
class headerCell: UITableViewCell {
    @IBOutlet weak var currencyHeaderLbl: customLabel!
    @IBOutlet weak var lineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lineView.backgroundColor = UIColor.placeholder
        currencyHeaderLbl.withType(type: .Bold, fontSize: 20, fontcolor: .buttonColor)
    }
}

//Address TableViewCell
class currencyCell: UITableViewCell {
    @IBOutlet weak var currencyNameLbl: customLabel!
    @IBOutlet weak var currencyLbl: customLabel!
    @IBOutlet weak var currencyValueLbl: customLabel!
    @IBOutlet weak var lineView: UIView!
     @IBOutlet weak var selectedImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lineView.backgroundColor = UIColor.placeholder
        currencyLbl.withType(type: .Medium, fontSize: 16, fontcolor: .white)
        currencyNameLbl.withType(type: .Medium, fontSize: 15, fontcolor: .white)
        currencyValueLbl.withType(type: .Default, fontSize: 15, fontcolor: .white)
    }
}

class CurrencyViewController: UIViewController {
    
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var headerLbl: customLabel!
    @IBOutlet weak var currencyTable: UITableView!
    
    let currencyArr = [["currency":"ARS","currencyname":"Argentina Pesos","currencyvalue":"AR$ 7.302.89"],["currency":"AUD","currencyname":"Australian Dollar","currencyvalue":"AU$ 276.32"],["currency":"BRL","currencyname":"Brazilian Real","currencyvalue":"R$ 799.32"],["currency":"CAD","currencyname":"Canadian Pesos","currencyvalue":"CA$ 259.65"]]
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
    }
    
    func themeAppearence(){
        self.view.backgroundColor = UIColor.primary
        self.currencyTable.backgroundColor = UIColor.clear
        headerLbl.withType(type: .Bold, fontSize: 22, fontcolor: .white)
        headerView.withType(type: .Default, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        headerLbl.text = Constants.string.currencyHeaderStr
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnAct(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//Table View Delegate
extension CurrencyViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else {
            return currencyArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath) as!  headerCell
            cell.currencyHeaderLbl.text = "1.00 BTC ="
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "currencyCell", for: indexPath) as!  currencyCell
            cell.currencyValueLbl.text = currencyArr[indexPath.row]["currencyvalue"]
            cell.currencyLbl.text = currencyArr[indexPath.row]["currency"]
            cell.currencyNameLbl.text = currencyArr[indexPath.row]["currencyname"]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.section == 0 {
            return 60
        }else {
            return 75
        }
    }
}
