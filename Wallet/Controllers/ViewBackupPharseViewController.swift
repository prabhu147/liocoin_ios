//
//  ViewBackupPharseViewController.swift
//  Wallet
//
//  Created by CSS on 04/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

class ViewBackupPharseViewController: UIViewController {
    
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var headerLbl: customLabel!
    @IBOutlet weak var qrImage: UIImageView!
    @IBOutlet weak var backupPharseLbl: customLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
    }
    
    func themeAppearence(){
        self.view.backgroundColor = UIColor.primary
        headerLbl.withType(type: .Bold, fontSize: 22, fontcolor: .white)
        headerView.withType(type: .Default, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        headerLbl.text = Constants.string.backupPharseHeaderStr
        qrImage.image = CommonClass.CreateQrCodeForyourString(string: "behave bone cannon confirm couple crane easily elder exotic grunt host level orbit over rate ridge senior shadow table truth unfold utility vapor voice")
        backupPharseLbl.withType(type: .Medium, fontSize: 20, fontcolor: .buttonColor)
        backupPharseLbl.text = "behave bone cannon confirm couple crane easily elder exotic grunt host level orbit over rate ridge senior shadow table truth unfold utility vapor voice"
    }
    
    @IBAction func backBtnAct(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
