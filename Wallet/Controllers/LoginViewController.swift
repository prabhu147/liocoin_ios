//
//  LoginViewController.swift
//  Wallet
//
//  Created by CSS on 01/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    //Outlets
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var headerLbl: customLabel!
    @IBOutlet weak var passwordTxt: customTextField!
    @IBOutlet weak var passwordBaseView: CustomView!
    @IBOutlet weak var emailTxt: customTextField!
    @IBOutlet weak var emailBaseView: CustomView!
    @IBOutlet weak var nextBtnOut: customButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
        headerLbl.text = Constants.string.login
        nextBtnOut.setTitle(Constants.string.login, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func themeAppearence()  {
        self.view.backgroundColor = UIColor.primary
        headerView.withType(type: .Default, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        emailTxt.withType(type: .Default, fontSize: 20, textField: emailTxt, fontcolor:.placeholder, placeholderTxt:Constants.string.email)
        passwordTxt.withType(type: .Default, fontSize: 20, textField: passwordTxt, fontcolor: .placeholder, placeholderTxt:Constants.string.password)
        emailBaseView.withType(type: .CornerRadius, BgColr: .cellBgColor, isBorder: false, borderWidth: 0.0, isshadow: true)
        passwordBaseView.withType(type: .CornerRadius, BgColr: .cellBgColor, isBorder: false, borderWidth: 0.0, isshadow: true)
        headerLbl.withType(type: .Default, fontSize: 22, fontcolor: .white)
        nextBtnOut.withType(type: .WithShadow, fontSize: 20, BgColor: .buttonColor, TextColor: .black)
    }
    
    @IBAction func backBtnAct(_ sender: Any) {
     self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closeBtnAct(_ sender: Any) {
     self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextBtnAct(_ sender: Any) {
     self.performSegue(withIdentifier: "HomeSegue", sender: self)
    }

}
