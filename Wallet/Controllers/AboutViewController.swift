//
//  AboutViewController.swift
//  Wallet
//
//  Created by CSS on 05/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var headerLbl: customLabel!
    @IBOutlet weak var configurationHeaderLbl: customLabel!
    @IBOutlet weak var versionHeaderLbl: customLabel!
    @IBOutlet weak var configurationValueLbl: customLabel!
    @IBOutlet weak var versionValueLbl: customLabel!
    @IBOutlet weak var websiteHeaderLbl: customLabel!
    @IBOutlet weak var termsOfServiceHeaderLbl: customLabel!
    @IBOutlet weak var websiteValueLbl: customLabel!
    @IBOutlet weak var termsOfServiceValueLbl: customLabel!
    @IBOutlet weak var privacyHeaderLbl: customLabel!
    @IBOutlet weak var whatisNewHeaderLbl: customLabel!
    @IBOutlet weak var privacyValueLbl: customLabel!
    @IBOutlet weak var whatisNewValueLbl: customLabel!
    @IBOutlet weak var copyRightHeaderLbl: customLabel!
    @IBOutlet weak var copyRightValueLbl: customLabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
        stringUpdate()
    }

    func themeAppearence(){
        self.view.backgroundColor = UIColor.primary
        headerLbl.withType(type: .Bold, fontSize: 22, fontcolor: .white)
        versionHeaderLbl.withType(type: .Medium, fontSize: 16, fontcolor: .white)
        configurationHeaderLbl.withType(type: .Medium, fontSize: 16, fontcolor: .white)
        websiteHeaderLbl.withType(type: .Medium, fontSize: 16, fontcolor: .white)
        termsOfServiceHeaderLbl.withType(type: .Medium, fontSize: 16, fontcolor: .white)
        privacyHeaderLbl.withType(type: .Medium, fontSize: 16, fontcolor: .white)
        whatisNewHeaderLbl.withType(type: .Medium, fontSize: 16, fontcolor: .white)
        copyRightHeaderLbl.withType(type: .Medium, fontSize: 16, fontcolor: .white)
        versionValueLbl.withType(type: .Medium, fontSize: 16, fontcolor: .white)
        configurationValueLbl.withType(type: .Medium, fontSize: 16, fontcolor: .white)
        websiteValueLbl.withType(type: .Medium, fontSize: 16, fontcolor: .blue)
        termsOfServiceValueLbl.withType(type: .Medium, fontSize: 16, fontcolor: .blue)
        privacyValueLbl.withType(type: .Medium, fontSize: 16, fontcolor: .blue)
        whatisNewValueLbl.withType(type: .Medium, fontSize: 16, fontcolor: .blue)
        copyRightValueLbl.withType(type: .Medium, fontSize: 18, fontcolor: .white)
        headerView.withType(type: .Default, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        headerLbl.text = Constants.string.aboutHeaderStr
    }
    
    func stringUpdate(){
        versionHeaderLbl.text = Constants.string.versionHedaerStr
        configurationHeaderLbl.text = Constants.string.configurationHederStr
        websiteHeaderLbl.text = Constants.string.websiteHedaerStr
        termsOfServiceHeaderLbl.text = Constants.string.termsofserviceheaderStr
        privacyHeaderLbl.text = Constants.string.privacyHedaerStr
        whatisNewHeaderLbl.text = Constants.string.whatsnewStr
        copyRightHeaderLbl.text = Constants.string.copyRightStr
        websiteValueLbl.text = "https://lio-coin.eu/"
        termsOfServiceValueLbl.text = Constants.string.termsofserviceValueStr
        versionValueLbl.text = "1.0"
        whatisNewValueLbl.text = Constants.string.whatsnewValueStr
        privacyValueLbl.text = Constants.string.privacyValueStr
        copyRightValueLbl.text = Constants.string.appName
        configurationValueLbl.text = "V3.127 received:sep  07 2018 07:23 PM"
    }
    
    @IBAction func backBtnAct(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
