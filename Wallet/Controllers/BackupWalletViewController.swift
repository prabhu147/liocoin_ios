//
//  BackupWalletViewController.swift
//  Wallet
//
//  Created by CSS on 04/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

class backupWalletCell:UITableViewCell {
    @IBOutlet weak var contentLbl: customLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        contentLbl.withType(type: .Bold, fontSize: 20, fontcolor: .white)
    }
}

class BackupWalletViewController: UIViewController {
    
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var headerLbl: customLabel!
    @IBOutlet weak var backupListTable: UITableView!
    var backupListArr = ["Back Wallet","View Backup Pharse"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
    }
    
    func themeAppearence(){
        self.view.backgroundColor = UIColor.primary
        self.backupListTable.backgroundColor = UIColor.clear
        headerLbl.withType(type: .Bold, fontSize: 22, fontcolor: .white)
        headerView.withType(type: .Default, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        headerLbl.text = Constants.string.backupWalletStr
    }
    
    @IBAction func backBtnAct(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK:- UITableView
extension BackupWalletViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return backupListArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "backupWalletCell", for: indexPath) as! backupWalletCell
        let separatorLine = UIImageView.init(frame: CGRect(x: 0, y: cell.frame.height-1, width: cell.frame.width, height: 1))
        separatorLine.backgroundColor = UIColor.cellBgColor
        cell.addSubview(separatorLine)
        cell.tintColor = UIColor.white
        cell.backgroundColor = UIColor.clear
        cell.contentLbl.text = backupListArr[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
         tablecViewAction(index: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath){
       tablecViewAction(index: indexPath.row)
    }
    
    func tablecViewAction(index:Int)  {
        switch index {
        case 0:
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "BackWalletViewController") as! BackWalletViewController
            self.navigationController?.pushViewController(controller, animated: true)
        case 1:
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "DisplayKeysViewController") as! DisplayKeysViewController
            self.navigationController?.pushViewController(controller, animated: true)
        default:break
        }
    }
}
