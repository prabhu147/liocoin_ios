//
//  RECEIVEViewController.swift
//  Wallet
//
//  Created by CSS on 03/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

class RECEIVEViewController: UIViewController {
    
    //OUTLETS
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var headerLbl: customLabel!
    @IBOutlet weak var txtBaseView: CustomView!
    @IBOutlet weak var currencyBtn: customButton!
    @IBOutlet weak var amtTxt: customTextField!
    @IBOutlet weak var qrBorderImage: UIImageView!
    @IBOutlet weak var qrImage: UIImageView!
    @IBOutlet weak var addressLbl: customLabel!
    @IBOutlet weak var addressHeaderLbl: customLabel!
    @IBOutlet weak var addressBaseView: CustomView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
        stringUpdate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func themeAppearence()  {
        self.view.backgroundColor = UIColor.primary
        headerView.withType(type: .Default, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        headerLbl.withType(type: .Bold, fontSize: 22, fontcolor: .white)
        txtBaseView.withType(type: .CornerRadius, BgColr: .white, isBorder: false, borderWidth: 0.0, isshadow: true)
        currencyBtn.withType(type: .WithOutCornerRadius, fontSize: 18, BgColor: .secondary, TextColor: .white)
        amtTxt.withType(type: .Default, fontSize: 18, textField: amtTxt, fontcolor: .black, placeholderTxt: "Enter Amount")
        qrImage.image = CommonClass.CreateQrCodeForyourString(string: "address")
        addressHeaderLbl.withType(type: .Bold, fontSize: 18, fontcolor: .white)
        addressLbl.withType(type: .Bold, fontSize: 14, fontcolor: .white)
        addressBaseView.withType(type: .CornerRadius, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
    }

    func stringUpdate()  {
        headerLbl.text = Constants.string.receiveStr
        addressHeaderLbl .text = Constants.string.accountStr
        currencyBtn.setTitle("LIO", for: .normal)
        addressLbl.text = "0xc3785ca6f36b9d2128ef74b0a63a40404138ba6b"
    }
    
    @IBAction func backBtnAct(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
