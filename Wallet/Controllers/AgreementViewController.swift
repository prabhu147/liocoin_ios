//
//  AgreementViewController.swift
//  Wallet
//
//  Created by CSS on 05/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

class AgreementHeaderCell: UITableViewCell {
    @IBOutlet weak var agreementHeaderLbl: customLabel!
    @IBOutlet weak var lineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lineView.backgroundColor = UIColor.placeholder
        agreementHeaderLbl.withType(type: .Medium, fontSize: 18, fontcolor: .white)
    }
}

class AgreementCell: UITableViewCell {
    @IBOutlet weak var agreementValueLbl: customLabel!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        agreementValueLbl.withType(type: .Medium, fontSize: 18, fontcolor: .white)
    }
}

class AgreementViewController: UIViewController {

    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var agreementTableView: UITableView!
    @IBOutlet weak var acceptBtn: customButton!
    @IBOutlet weak var declineBtn: customButton!
    @IBOutlet weak var headrLbl: customLabel!
    
    var agreementArr = ["View Terms of Service","View Privacy Policy"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func themeAppearence()  {
        self.view.backgroundColor = UIColor.primary
        self.agreementTableView.backgroundColor = UIColor.clear
        acceptBtn.setTitle(Constants.string.accept, for: .normal)
        declineBtn.setTitle(Constants.string.declineStr, for: .normal)
        headerView.withType(type: .Default, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        acceptBtn.withType(type: .WithShadow, fontSize: 18, BgColor: .buttonColor, TextColor: .white)
        declineBtn.withType(type: .WithShadow, fontSize: 18, BgColor: .buttonColor, TextColor: .white)
        headrLbl.withType(type: .Bold, fontSize: 22, fontcolor: .white)
        headrLbl.text = Constants.string.agreeHeaderstr
    }

}

//Table View Delegate
extension AgreementViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else {
            return agreementArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AgreementHeaderCell", for: indexPath) as!  AgreementHeaderCell
           cell.agreementHeaderLbl.text = Constants.string.agreestr
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AgreementCell", for: indexPath) as!  AgreementCell
            let separatorLine = UIImageView.init(frame: CGRect(x: 0, y: cell.frame.height-1, width: cell.frame.width, height: 1))
            separatorLine.backgroundColor = UIColor.placeholder
            cell.addSubview(separatorLine)
            cell.tintColor = UIColor.white
            cell.agreementValueLbl.text = agreementArr[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.section == 0 {
            return 120
        }else{
            return 60
        }
    }
}
