//
//  PasscodeViewController.swift
//  Wallet
//
//  Created by CSS on 04/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

@objc public protocol EightDigitPinDelegate: class {
    @objc func callsixDigitPinService(pinStatus:String ,passcodeStr:String)
}

class PasscodeViewController: UIViewController {
    
    enum passcodeStatus: String {
        case new = "SET PIN"
        case verify = "RE-ENTER PIN"
        case ready = "Verified"
        case wrong = "Wrong passcode"
    }
    
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var headerLbl: customLabel!
    @IBOutlet weak var firstNum: UIImageView!
    @IBOutlet weak var secondNum: UIImageView!
    @IBOutlet weak var thirdNum: UIImageView!
    @IBOutlet weak var fourthNum: UIImageView!
    @IBOutlet weak var passCodeHeaderLbl: customLabel!
    
    var numsIcons: [UIImageView]?
    var passcode: String = ""
    var repeatedPasscode: String = ""
    var status: passcodeStatus = .new
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numsIcons = [firstNum, secondNum, thirdNum, fourthNum]
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
    }
    
    func themeAppearence(){
        self.view.backgroundColor = UIColor.primary
        headerLbl.withType(type: .Bold, fontSize: 22, fontcolor: .white)
        headerView.withType(type: .Default, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        headerLbl.text = Constants.string.settingsHeaderStr
        passCodeHeaderLbl.withType(type: .Bold, fontSize: 20, fontcolor: .white)
        passCodeHeaderLbl.text = Constants.string.passCodeHeaderStr
    }
    
    func changePasscodeStatus(_ newStatus: passcodeStatus) {
        status = newStatus
        if status == .wrong {
            repeatedPasscode = ""
            changeNumsIcons(0)
        } else if status == .ready {
            ApiHelper.apiIndstance.AlertMessage(message: AlertString.string.disablelaunchingPinMessage)
            self.navigationController?.popViewController(animated: true)
        } else if status == .verify {
            passCodeHeaderLbl.text = Constants.string.confirmPin
            changeNumsIcons(0)
        }
    }
    
    func changeNumsIcons(_ nums: Int) {
        switch nums {
        case 0:
            for i in 0...(numsIcons?.count)!-1 {
                self.numsIcons![i].image = UIImage(named: "circle-outline")
            }
        case 4:
            for i in 0...nums-1 {
                self.numsIcons![i].image = UIImage(named: "White_dot")
            }
        default:
            for i in 0...nums-1 {
                self.numsIcons![i].image = UIImage(named: "White_dot")
            }
            for i in nums...(numsIcons?.count)!-1 {
                self.numsIcons![i].image = UIImage(named: "circle-outline")
            }
        }
    }
    
    @IBAction func numberTouchedDown(_ sender: UIButton) {
        UIView.animate(withDuration: 0.05,
                       animations: {
                        sender.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)},
                       completion: nil)
    }
    
    @IBAction func touchAborted(_ sender: UIButton) {
        UIView.animate(withDuration: 0.05) {
            sender.transform = CGAffineTransform.identity
        }
    }
    
    @IBAction func touchDragInside(_ sender: UIButton) {
        UIView.animate(withDuration: 0.05,
                       animations: {
                        sender.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)},
                       completion: nil)
    }
    
    @IBAction func numberPressed(_ sender: UIButton) {
        let number = sender.currentTitle!
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.05) {
                sender.transform = CGAffineTransform.identity
            }
        }
        if status == .new {
            passcode += number
            changeNumsIcons(passcode.count)
            if passcode.count == 4 {
                let newStatus: passcodeStatus = .verify
                changePasscodeStatus(newStatus)
            }
        } else if status == .verify {
            repeatedPasscode += number
            changeNumsIcons(repeatedPasscode.count)
            if repeatedPasscode.count == 4 {
                let newStatus: passcodeStatus = repeatedPasscode == passcode ? .ready : .wrong
                changePasscodeStatus(newStatus)
            }
        } else if status == .wrong {
            changePasscodeStatus(.verify)
            repeatedPasscode += number
            changeNumsIcons(repeatedPasscode.count)
        }
    }
    
    @IBAction func clearButtonPressed(_ sender: UIButton) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.05) {
                sender.transform = CGAffineTransform.identity
            }
        }
        switch status {
        case .new:
            if passcode != "" {
                passcode = ""
                changeNumsIcons(0)
            }
        default:
            if repeatedPasscode != "" {
                repeatedPasscode = ""
                changeNumsIcons(0)
            }
        }
        
    }
    
    @IBAction func deletePressed(_ sender: UIButton) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.05) {
                sender.transform = CGAffineTransform.identity
            }
        }
        switch status {
        case .new:
            if passcode != "" {
                passcode.removeLast()
                changeNumsIcons(passcode.count)
            }
        default:
            if repeatedPasscode != "" {
                repeatedPasscode.removeLast()
                changeNumsIcons(repeatedPasscode.count)
            }
        }
    }
    
    @IBAction func backBtnAct(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}



