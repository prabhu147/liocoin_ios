//
//  ConfirmPharseViewController.swift
//  Wallet
//
//  Created by CSS on 05/10/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import UIKit

class ConfirmPharseViewController: UIViewController {
    
    @IBOutlet weak var successContentLbl: customLabel!
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var headerLbl: customLabel!
    @IBOutlet weak var headerContentLbl: customLabel!
    @IBOutlet weak var nextBtnOut: customButton!
    @IBOutlet weak var confirmPharseView: CustomView!
    @IBOutlet weak var successHedaerLbl: customLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        themeAppearence()
    }
    
    func themeAppearence(){
        self.view.backgroundColor = UIColor.primary
        headerLbl.withType(type: .Bold, fontSize: 22, fontcolor: .white)
        headerContentLbl.withType(type: .Medium, fontSize: 20, fontcolor: .white)
        successHedaerLbl.withType(type: .Medium, fontSize: 20, fontcolor: .white)
        successContentLbl.withType(type: .Medium, fontSize: 20, fontcolor: .white)
        headerView.withType(type: .Default, BgColr: .secondary, isBorder: false, borderWidth: 0.0, isshadow: true)
        confirmPharseView.withType(type: .CornerRadius, BgColr: .white, isBorder: false, borderWidth: 0.0, isshadow: true)
        nextBtnOut.withType(type: .WithShadow, fontSize: 20, BgColor: .buttonColor, TextColor: .black)
        nextBtnOut.setTitle(Constants.string.finishStr, for: .normal)
        headerLbl.text = Constants.string.backupPharsetStr
        headerContentLbl.text = Constants.string.successHedaerStr
        successHedaerLbl.text = Constants.string.successStr
        successContentLbl.text = Constants.string.successContentStr
    }
    
    @IBAction func backBtnAct(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextBtnAct(_ sender: Any) {
    }


}
